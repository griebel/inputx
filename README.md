# inputx
Latex package for an extended input command

Now there is one important new command available: \inputpaths

\inputpaths specifies the directories where \include and \input can look for the given file. By default \inputpaths is empty and therefore \include and \input have the same behavior as the normal commands.

The redefined \include and \input use the root-directory. If the file is not in the root-directory than the commands looking for the file in the specified directories.

Example:
```
File structure:
./main.tex
./file2.tex
./dir1/file1.tex
./dirN/file3.tex
```

Contents of file1.tex:
```TeX
\usepackage{inputx}
...
\inputpaths{dir1,...,dirN}
...
\begin{document}

\include{file1}
\input{file2}

\end{document}
```

Contents of dir1/file1.tex:
```TeX
\input{file3}
\input{dirN/file3}
```

Contents of file2.tex:
```TeX
file2: Hello World!
```

Contents of dirN/file3.tex:
```TeX
file3: Hello World!
```

Output:
```
file3: Hello World!
file3: Hello World!
file2: Hello World!
```

*This package is inspired from the answer of blackstev in this question https://tex.stackexchange.com/questions/4602/how-to-make-the-main-file-recognize-relative-paths-used-in-the-imported-files*
